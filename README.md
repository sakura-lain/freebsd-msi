# Installation et maintenance de FreeBSD sur un MSI U130

**[Voir le Wiki >>>](https://gitlab.com/sakura-lain/freebsd-msi/wikis/home)**

Voir aussi, pour le même ordinateur :

- [Debian](https://gitlab.com/sakura-lain/debian-msi)
- [CentOS](https://gitlab.com/sakura-lain/centos-msi)

## Configuration générale

- FreeBSD 11.1 (mise à jour en 11.2 puis en 11.3 et enfin en 12.1). 
- L'ordinateur possédait à l'origine un clavier Qwerty modifié en Azerty, d'où la nécessité de remapper les touches < et > sur la touche Windows.
- [Une entrée FreeBSD est créée manuellement dans le Grub de la Debian voisine](https://gitlab.com/sakura-lain/debian-msi/wikis/Ajout-d'une-entr%C3%A9e-FreeBSD-dans-Grub2).
- Configuration graphique : pas de gestionnaire de connexion + Lumina

## Applications installées

- Arduino
- Celestia
- Filezilla
- Firefox
- Git
- Grsync
- Inkscape
- Keepass
- Links
- Midnight Commander
- Python 3 et ses outils de développement
- QBitTorrent
- Stellarium
- The Gimp
- Transmission
- Vim
- VLC
- XFCE Terminal
- Xpdf
- XScreenSaver
- Xterm
- Zathura
