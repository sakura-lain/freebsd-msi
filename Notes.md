# Installer FreeBSD 11.1 sur un MSI U130

- FreeBSD 11.1 (mise à jour en 11.2 puis en 11.3 et enfin en 12.1). 
- L'ordinateur possédait à l'origine un clavier Qwerty modifié en Azerty, d'où la nécessité de remapper les touches < et > sur la touche Windows.
- Une entrée FreeBSD est manuellement créée dans le Grub de la Debian voisine.

Voir aussi, pour le même ordinateur :

- [Debian](https://gitlab.com/sakura-lain/debian-msi)
- [CentOS](https://gitlab.com/sakura-lain/centos-msi)

## Installation et maintenance :

### Installation :

[Installation de FreeBSD - OpenClassrooms](https://openclassrooms.com/courses/a-la-decouverte-d-unix-freebsd/installation-de-freebsd)

- Activer `moused`, `ntpd`, `powerd` en plus des choix par défaut.
- Options de sécurité : [Forums FreeBSD](https://forums.freebsd.org/threads/59599/) 
- Installer `additionnal doc` et `system source tree`, `handbook` également (à la fin)

Voir aussi : [FreeBSD Handbook - Install](https://www.freebsd.org/doc/fr/books/handbook/install.html) 

### Passer le système et la console en français (accents, apostrophes, etc.) :

- afficher ses paramètre de langue : `locale`
- modifier le fichier `/home/username/.login_conf` comme suit, pour reproduire les paramètres `locale` qui fonctionnent sous Linux :

```
me:\
        :charset=UTF-8:\
        :lang=fr_FR.UTF-8:\
        :setenv=LC_ALL=fr_FR.UTF-8:\
        :setenv=LC_CTYPE=fr_FR.UTF-8:\
        :setenv=LC_NUMERIC=fr_FR.UTF-8:\
        :setenv=LC_TIME=fr_FR.UTF-8:\
        :setenv=LC_COLLATE=fr_FR.UTF-8:\
        :setenv=LC_MONETARY=fr_FR.UTF-8:\
        :setenv=LC_MESSAGES=fr_FR.UTF-8:\
        :setenv=LC_PAPER=fr_FR.UTF-8:\
        :setenv=LC_NAME=fr_FR.UTF-8:\
        :setenv=LC_ADDRESS=fr_FR.UTF-8:\
        :setenv=LC_TELEPHONE=fr_FR.UTF-8:\
        :setenv=LC_MEASUREMENT=fr_FR.UTF-8:\
        :setenv=LC_IDENTIFICATION=fr_FR.UTF-8:
```

- Redémarrer

### Logiciels portés :

Dossier `/usr/ports`. Quelques exemples :

- `/usr/ports/astro` : logiciels d'astronomie
- `/usr/ports/devel` : outils de développement
- `/usr/ports/french` : traductions françaises (par exemple : `libreoffice`)
- `/usr/ports/graphics` : outils de graphiqme (par exemple : `gimp-app`)
- `/usr/ports/lang` : langages de programmation
- `/usr/ports/misc` : divers (par exemple : `mc`)
- `/usr/ports/www` : outils web (par exemple : `webkit-gtk2`)
- `/usr/ports/X11` : interfaces graphiques

Commandes `portmaster` et `portsnap`.

Compilation manuelle avec `make` :

```
make 
make install clean
make deinstall
```

- [FreeBSD Handbook](https://www.freebsd.org/doc/fr_FR.ISO8859-1/books/handbook/ports-using.html)
- [Portsnap - FreeBSD Handbook](https://www.freebsd.org/doc/fr_FR.ISO8859-1/books/handbook/updating-upgrading-portsnap.htm)

### Mises à jour :

`uname -a` ou `freebsd-version` pour connaître la version.

#### La commande `freebsd-update` (mise à jour du système) :

```
freebsd-update fetch #Récupère les dépôts et affiche les changements
freebsd-update install #Applique les changements
freebsd-update rollback #Revient sur les changements
freebsd-update upgrade -r 11.3-RELEASE #Upgrade vers la version mineure 11.3
```

Suivre les instructions après une mise à jour de version, il faut lancer `freebsd-update install`, puis redémarrer, puis le relancer.

- [FreeBSD Handbook](https://www.freebsd.org/doc/handbook/updating-upgrading-freebsdupdate.html)
- [Version 11.2 - FreeBSD handbook](https://www.freebsd.org/releases/11.2R/installation.html)
- [Liste des versions](https://www.freebsd.org/releases/)
- [Forums FreeBSD](https://forums.freebsd.org/threads/freebsd-update-fetch-src-component-not-installed.63956/)
- [Jump versions - Forums FreeBSD](https://forums.freebsd.org/threads/upgrade-from-9-3-to-11-2.67950/)
- [Upgrade from 11.3 to 12 - cyberciti](https://www.cyberciti.biz/open-source/freebsd-12-released-here-is-how-to-upgrade-freebsd/)

#### La commande `pkg` (installation et mise à jour des paquets) :

```
pkg install package #Installe le paquet package
pkg update  #Met à jour les dépôts
pkg upgrade #Met à jour les dépôts et les paquets
pkg clean #Nettoie le cache
pkg autoremove
pkg-static upgrade -f #Remet à jour les dépôts et les paquets après une mise à jour vers une version majeure
```

- [FreeBSD handbook](https://www.freebsd.org/doc/handbook/pkgng-intro.html)
- [FreeBSD Forums](https://forums.freebsd.org/threads/upgrading-the-whole-system-including-packages.61618/)
- [pkg clean - FreeBSD lists](https://lists.freebsd.org/pipermail/freebsd-stable/2015-August/082981.html)

#### Exemple : mise à jour de Freebsd 11.3 vers 12.1 :

Il est possible de faire des mises à jour majeures en sautant les versions.

[Cyberciti](https://www.cyberciti.biz/open-source/freebsd-12-released-here-is-how-to-upgrade-freebsd/)

##### Procédure :

```
freebsd-update upgrade -r 12.1-RELEASE
freebsd-update install
``` 

Reboot

```
pkg-static upgrade -f
freebsd-update install
portmaster -af
portsnap fetch
portsnap extract
portsnap update
```

Nécessité de désinstaller/réinstaller plusieurs logiciels, y compris par les ports.

##### Problème d'absence de fichier `/usr/local/bin/perl5.30.1` nécessaire à `gimp-app`, empêchant la mise à jour complète des logiciels gérés par `pkg` :

Après avoir tenté pas mal de choses et après compilation de perl via les ports, le problème persiste toujours. Curieusement c'est bien la version 2.30.1 qui est compilée, pkg la détecte aussi mais quand on affiche la version de Perl elle est en 5.30.0.

Un petit bricolage permet de contourner le problème :

```
ln -s /usr/local/bin/perl5.30.0 /usr/local/bin/perl5.30.1
```

##### Libreoffice ne démarre pas et bloque sur l'erreur "failed to updatefile: .config/libreoffice/4/user/extensions/bundled/lastsynchronized" :

Les procédures consistant à mofifier les droits du fichier ou son contenu n'ont pas fonctionné.

- Désinstaller LibreOffice
- Supprimer le dossier `$HOME/.config/libreoffice`
- Réinstaller LibreOffice
- Installer la traduction française via les ports.

## Interfaces graphiques :

### Xfce :

- Installer Xfce : `pkg install xfce` 
- Installer Xorg : `pkg install xorg`
- Lancer avec `startx` (pour root) : `echo "exec /usr/local/bin/startxfce4 --with-ck-launch" > ~/.xinitrc`
- Lancer XFCE avec un utilisateur lambda : `startxfce4`
- XFCE n’est pas joli du tout ici, ni très pratique : mauvaise résolution, tout en anglais avec clavier qwerty.
- Installer `Slim` et ajouter dans `Loader.conf` ce qui suit :

```
dbus_enable="YES"
hald_enable="YES"
slim_enable="YES"
```

#### Paramétrer Xmodmap dans Xorg :

- Installer `Xorg`
- Installer` Xmodmap`
- Pour activer les touches < et > sur la touche Windows, créer dans `/root` et dans chaque `/home/user` un fichier `.Xmodmap` contenant la ligne :
    
```
keycode 115 = 60 62
```
	
- Créer dans `/home` un fichier `Xmodmap` identique à celui utilisé dans Linux
- Lancer `/home/Xmodmap` au démarrage de `Xorg`

#### Paramétrer le clavier Azerty dans Xorg :

- Installer `Xorg`
- S’il n’existe pas, créer un répertoire `xorg.conf.d` dans `/usr/local/etc/x11`
- Y ajouter un fichier `10-keyboard.conf` contenant le texte :

```
Section "InputClass"
    	Identifier "Keyboard0"
      	Driver "kbd"
    	MatchIsKeyboard "on"
    	#Option "XkbModel" "pc105"
    	Option "XkbRules" "xorg"
    	Option "XkbLayout" "fr"
    	Option "XkbVariant" "oss"
EndSection
```

#### Paramétrer le touchpad :

- Dans `/usr/local/etc/X11/xorg.conf.d`, ajouter un fichier `10-mouse.conf` pour la souris contenant le texte :

```
Section "InputClass"
    	Identifier "Mouse0"
    	Driver "mouse"
    	MatchIsPointer "on"
EndSection
```
	
*NB* : Cette procédure est inutile avec lumina.	
	
[Tux Family](https://avignu.wiki.tuxfamily.org/doku.php?id=documentation:bsd:freebsd:clavier-fr)

#### Paramétrer Xscreensaver pour qu'il verrouille l'écran :

Dans le fichier `$HOME/.xscreensaver`, passer la valeur de `lock` à `True` :

```
lock:   True
```

### Autres problèmes graphiques

- `Lightgm` ne fonctionne pas : [FreeBSD Handbook - X11](https://www.freebsd.org/doc/handbook/x11-wm.html) 
- Si problème au lancement de `Xorg` lié à `D-Bus` : `dbus-uuidgen –ensure`
- Résolution : installer `xf86-video-intel` et éventuellement `xf86-video-vesa`
- On peut aussi se passer de gestionnaire de connexion et lancer l'interface graphique par `startx`.

### Lumina :

#### Xmodmap :

Pour que Xmodmap fonctionne comme on l’a programmé plus haut (touches < et > sur la Windows Key) il faut éditer `$HOME/.xinitrc` comme suit :

```
#!/bin/sh

userresources=$HOME/.Xresources
usermodmap=$HOME/.Xmodmap
sysresources=/etc/X11/xinit/.Xresources
sysmodmap=/etc/X11/xinit/.Xmodmap

# merge in defaults and keymaps

if [ -f $sysresources ]; then
  xrdb -merge $sysresources
fi
if [ -f $sysmodmap ]; then
  xmodmap $sysmodmap
fi
if [ -f $userresources ]; then
  xrdb -merge $userresources
fi
if [ -f $usermodmap ]; then
  xmodmap $usermodmap
fi

# Start the window manager:
if [ -z "$DESKTOP_SESSION" -a -x /usr/bin/ck-launch-session ]; then
  exec ck-launch-session start-lumina-desktop
else
  exec start-lumina-desktop
fi
```

- Supprimer le raccourci associé à la Windows Key dans Lumina : préférences > configurer le bureau > Desktop Defaults > Keyboard shortcuts > clear shortcut associé à la touche 115 > apply change

#### Autres problèmes du bureau Lumina :

- Déplacer une fenêtre trop grande pour la redimensionner : alt + clic gauche
- En cas de crash (ex : après une extinction brutale), réinitialiser l’interface en supprimant `.cache`, `.config`, `.local` (éventuellement `.fluxbox` ou autre dossier similaire d’un gestionnaire de fenêtres utilisé par Lumina). 
- En cas de (très) gros crash, on peut réinitialiser le compte utilisateur en supprimant `/home/username`
- [Slackware](http://www.slackware.com/~alien/slackbuilds/lumina/build/lumina.xinitrc)
- [Lumina Desktop](https://lumina-desktop.org/handbook/luminaconfig.html)

### Config graphique finale :

Pas de gestionnaire de connexion + Lumina

## Comptes utilisateurs et groupes :

### Gestion des utilisateurs

- Commande générale : `pw`
- Déconnecter un utilisateur : `exit`
- Supprimer un utilisateur (ne pas le faire avec un membre du groupe `wheel`) : `rmuser` (attention, le système prévient qu’il le connaît toujours quand on essaie de le recréer)

### Groupe wheel

- Un utilisateur lambda ne peut pas passer `su` par défaut. Pour cela, il doit être dans le groupe `wheel` (déconseillé pour des raisons de sécurité).
- Ajouter un utilisateur à `wheel` sans en supprimer root : 

```
pw groupmod wheel -m username
```

### Retirer un utilisateur d’un groupe (lightdm) : 

- [FreeBSD Handbook](https://www.freebsd.org/doc/fr/books/handbook/users-modifying.html)
- [Cyberciti](https://www.cyberciti.biz/faq/howto-freebsd-add-a-user-to-wheel-group/) 
- [FreeBSD Handbook](https://www.freebsd.org/doc/handbook/users-synopsis.html)
- [Linuxtechi](https://www.linuxtechi.com/linux-commands-to-manage-local-accounts/)
- [Nico Largo](https://blog.nicolargo.com/2011/10/ajouter-un-utilisateur-a-un-groupe-sous-gnulinux.html)
- [Comment ça marche](http://www.commentcamarche.net/forum/affich-28934565-groupe-primaire-et-secondaire)

## Périphériques de stockage

### Monter un périphérique externe avec un utilisateur lambda :

- Dans le dossier `/mnt`, créer un dossier `./username` (puis éventuellement un sous-dossier pour chaque périphérique à monter). Le dossier se crée en root mais doit appartenir à l’utilisateur (username) et à son groupe (username) : 

```
chown username:username /mnt/username
```
(ou chown -R s’il y a des sous-dossiers)

- monter le dossier en root. Pour une clé USB (ici un périphérique monté sur da0 ne comprenant qu’une partition da0s1 en FAT32) : 

```
mount -t msdosfs -o -m=644,-M=755 /dev/da0s1 /mnt/username
```

- Se rendre dans `/mnt/username` avec l’utilisateur username
- Démonter en root : `umount /mnt/username`
- Si un message d’erreur apparaît indiquant que le préiphérique est occupé, c’est que qqe chose y est ouvert, y compris un shell

[OpenClassrooms](https://openclassrooms.com/courses/a-la-decouverte-d-unix-freebsd/acces-aux-cles-usb)

### Lire une partition NTFS :

- Installer `fusefs-ntfs`
- Identifier le volume à monter dans `/dev` (ici : `ada0s3`)
- Taper la commande `kldload fuse` (ou pour un chargement au démarrage inscrire `fuse_load="YES"` dans le fichier `/boot/loader.conf`)
- Taper la commande :

```
ntfs-3g /dev/ada0s3 /mnt
```

- Parcourir le dossier avec `cd /mnt`
- Quitter le dossier avec `cd`
- Démonter avec `umount /mnt`

[FreeBSD Forums](https://forums.freebsd.org/threads/5989/)

## Réseau

### Wifi :

- Installer `PyGTK` et `Doas`
- [Installer `Networkmgr` - GhostBSD](https://github.com/GhostBSD/networkmgr/blob/master/README.md)
- S’assurer que `/usr/local/etc/doas.conf` existe. Si non, le créer.
- Y inscrire :

```
permit :wheel
permit nopass keepenv :wheel cmd netcardmgr
permit nopass keepenv :wheel cmd detect-nics
permit nopass keepenv :wheel cmd detect-wifi
permit nopass keepenv :wheel cmd ifconfig
permit nopass keepenv :wheel cmd service
permit nopass keepenv :wheel cmd wpa_supplicant
permit nopass keepenv root
```

- Ajouter deux lignes à ala fin de `/etc/rc.conf` et redémarrer :

```
wlans_ral0="wlan0"
ifconfig_wlan0="WPA DHCP"
```

- Trouver des réseaux : 

``` 
# ifconfig wlan0 list scan
```

- Démarrer le service :

```
service netif start
```

[FreeBSD Handbook](https://www.freebsd.org/doc/handbook/network-wireless.html)

#### WPA Supplicant

On peut ajouter un réseau manuellement dans le fichier `/etc/wpa_supplicant.conf`.

- Pour un réseau WPA :

```
network={
        ssid="ssid_name"
        psk="psk_key"
}
```

- Pour un réseau Wep :

```
network={
        ssid="ssid_name"
        bssid=router_mac_address
        key_mgmt=NONE
        wep_tx_keyidx=0
        wep_key0=wep_key
}
```

Les paramètres sont immédiatement pris en compte par le système. Si ce n'est pas le cas, redémarrer.

Commandes :

- `wpa_cli reconfigure`
- `systemctl restart networking.service`

### Erreurs "My unqualified hostname [...]" etc.

Il faut juste ajouter le `hostname` de la machine à la fin de la ligne configurant la boucle locale dans `/etc/hosts` et s'assurer que `nsswitch.conf` est paramétré pour lire le fichier `hosts`.

```
127.0.0.1  localhost localhost.my.domain hostname
```

## Éteindre : 
- `shutdown now`
- Pour permetter à un utilisateur d’éteindre, l’ajouter au groupe `operator`.
- Si un message d’erreur invitant à se reconnecter à un shell apparaît, c’est probablement qu’un logiciel n’est pas fermé correctement, un fichier ouvert par un utilisateur, un périphérique externe monté… L’option `-p` permet de forcer l’extinction, mais avec un gros risque de perte de données (ex : données non inscrites sur un périphérique NTFS monté, même si elles ont été enregistrées auparavant)

[FreeBSD Handbook](https://www.freebsd.org/doc/fr/books/handbook/boot-shutdown.html )

/!\ Attention à bien tout fermer et tout démonter avant d’éteindre ! /!\

## Midnight Commander :

- Installer Midnight Commander : `pkg install mc`
- Lancer Midnight Commander : `mc`

[Utiliser Midnight Commander - Trembath](http://www.trembath.co.za/mctutorial.html)  

## Applications installées :

- Firefox
- Vim
- xpdf
- Transmission
- QBitTorrent
- VLC
- Filezilla
- Links
- Zathura
- The Gimp
- Grsync
- Midnight Commander
- Xterm
- XFCE Terminal
- Keepass
- XScreenSaver
- Stellarium
- Celestia
- Arduino

## Autres (notes rapides)

- Retirer un groupe ?
après config pour root
- trouver pilotes pour résolution ?
- Pb touche <> (console)
- Résolution console
- Partitionnage manuel ?